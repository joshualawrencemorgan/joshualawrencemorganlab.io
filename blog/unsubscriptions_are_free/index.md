# PicoCTF: Unsubscriptions Are Free

## The Challenge

The next challenge is titled "unsubscriptions-are-free". It seems to be another heap expoitation challenge with a menu just like [Are You Root](../areyouroot/index.md).

## The Tools
### PWNINIT

pwninit is a tool for automating starting binary exploit challenges that uses patchelf to fix binaries. It allows us to send and recieve lines inside of a python script.

### Ghidra

I actually didn't crack my disassembler for this one but I will keep advocating that it's a great tool to gain a better understanding of a binary.

## The Solve

### Quick analysis

Like always I wanted to just jump into the execution. Running prompts a menu with a few options. A quick check shows that a couple of them just `puts` a string and return. Other options (s,i, and l) change the function pointer in the user struct which is later called as part of main. An interesting design decision that leads to an error. Due to improper intialization of the user struct (deja vu) the function pointer can be called before it is set by following the (l)eave option. If this action is taken before any other function is called it results in a segfault, otherwise, it will cause the previous function to be executed.

### Testing

We can test this pretty easily.

![Segfault](img/loptioncrash.png)

Despite being pretty glaring and simple to do it gives a lot of information about the binary and how we might exploit it. The Segfault is occuring because we are trying to access memory we shouldn't be accessing. This means that we just need to find a way for the function pointer to point something we want it to. 

Lucky for us it's also pretty glaring where we need to go. The first function in `vuln.c` looks like this:

![exploitgobrrr](img/exploitgobrrr.png)

A pretty straight forward function printing out the flag that we can trigger by using the `s` option from the menu. Now we know the what and where it's just a matter of finding the how.

### Getting the flag

Luckily, the menu also allows us to place anything we want in the heap with the `l` option. We need to use our pwninit tool to get bytes that we want in the right order.

![stringtoheap](img/stringtoheap.png)

Awesome, but... we need the binary to do something with that address. This is where we take advantage of the programmers mistakes. The program never sets the user struct to NULL and then references the structure again in other functions.

![badccode](img/badc.png)

This allows us to free the user struct and then immediately malloc a string which happens to be the same size. This will give us control of memory that is unintentionally used as a function pointer. Here is the user structure before free.

![User Structure](img/userstruct.png)

Now after freeing and using the `l` option to malloc a new string.

![String where User was](img/stringinstructspot.png)

Which leads to a crash at our provided position.

![Deadbeef crash](img/crashondeadbeef.png)

Now all thats left is to provide the address of our flag function instead of 0xdeadbeef. We could hard code this but as you'll see from my [solve.py](solve.py) script I created a function that grabs the leaded address and puts it into a payload for you.

### Conclusion

A pretty good challenge that tested our understanding of heap exploitations. I enjoyed this one especially having just done [Are You Root](../areyouroot/). I also took this oppurtunity to improve my pwninit template and debugging skills.

Flag: picoCTF{d0ubl3_j30p4rdy_1e154727}