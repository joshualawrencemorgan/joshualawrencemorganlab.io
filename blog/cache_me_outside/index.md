# PicoCTF: Cache Me Outside

[Link to challenge](https://play.picoctf.org/practice/challenge/146)

## The Challenge

The next challenge is titled "Cache Me Outside" and also comes with the tag - binary exploitation. The description hints that the title is relevant to the program and how much we understand heap allocations. Attached is a makefile, an executable called heapedit, libc.so.6 and the command line instruction *nc mercury.picoctf.net 17612*.

## The Solve

### Quick analysis

With no context besides our hint I jumped into the challenge by executing the provided binary *heapedit*.

Segmentation fault.

Well that's weird. I would normally try to pull down the binary again and see if the file got messed up but this challenge came with a Makefile and a libc. That lead me to think there was something going on with how the binary was compiled. From the makefile:
```bash
gcc -Xlinker -rpath=./ -Wall -m64 -pedantic -no-pie --std=gnu99 -o heapedit heapedit.c
```
We don't have heapedit.c so we won't be able to rebuild the executable using this makefile. However, the inclusion of *-rpath* in the build made me remember that the challenge comes with a libc. I decided to follow a tip I received about pwninit.


### PWNINIT

pwninit is a tool for automating starting binary exploit challenges that uses patchelf to fix binaries. *NOTE:* Make sure to not have the pwninit binary in the directory when running it or else it might try to patch itself.

```bash
$ pwninit
bin: ./heapedit
libc: ./libc.so.6

fetching linker
setting ./ld-2.27.so executable
copying ./heapedit to ./heapedit_patched
running patchelf on ./heapedit_patched
```

### Ghidra
Now we're in business right?! Not quite. We still get a segfault so lets throw the binary into ghidra to see if anything weird is going on. One problem was quite glaring.
> local_90 = fopen("flag.txt","r");

Ghidra shows that a file called flag.txt is require for the program to run. A quick echo into that file and we should be set. But before switching back to the terminal I took a glance at the rest of main.

![ghidra screenshot of main](img/cache_ghidra.png)

A quick glance shows a series of mallocs followed by two frees and then another malloc. This reminded me of some Windows heap exploitation that I practiced where the heap manager would reserve a cache of memory and by mallocing a string of the same size you could potentially get one of the same blocks again. Looking a the print at the end seems to indicate that this challenge and heap manager follow similar concepts. Faced with the option of studying this specific heap manager or getting hands on with the now working binary, I took the easier path.

### Testing
```text
$ ./heapedit_patched 
You may edit one byte in the program.
Address: 0
Value: 0
t help you: this is a random string.
```

### Getting the flag

Alright, the binary is now running! It seems to ask for an address and then allows us change one byte at that location. It seems that the address is an offset from from the first malloc.
```C
local_98 = (undefined8 *)malloc(0x80);
if (local_a0 == (undefined8 *)0x0) {
    local_a0 = local_98;
}
.
.
printf("Address: ");
_isoc99_scanf("%d",&local_a8);
.
.
*(undefined *)((long)local_a8 + (long)local_a0) = local_a9;
```
Okay, great. Now, what address and what location? To find out I fired up gdb with gef to get some information about the heap. The first challenge will be to find that *local_a0 value that is our reference for later in the program. This can be accomplished by stepping through the program until it returns from the first malloc and then printing the return.

![return from first malloc](img/firstlocation_cache.png)

Sweet. Now time to see if it really is similar to the Windows Heap manager. Let's just jump until right after the first free.

![all of the chunks after first free](img/firstfree_cache.png)

Look all those malloced strings with the flag and the one bad string. It looks like the flag at 0x603800 has already been freed and when we check *heap bins* we find that it is in the tcache.

![our flag in the heap bin](img/heapbins_cache.png)
If we continue to after the next free we can get a look at everything right before the program asks for our input.

![after second free; two in the bin](img/secondfree_cache.png)

We know if we continue the program will malloc again and receive the chunk at 0x603890 because of our output from before. We'd rather have the flag than *'t help you: this is a random string'*. So we need to find whatever is pointing to the 0x603890 and change it to 0x603800 which nicely enough requires us to change one byte at one location.

![tcache pointer](img/tcache_cache.png)

Bam, a address in the heap that we can change just one byte of to get it to point to our flag. A quick calculation of the offset that ( we don't even have to leave gdb, p/d 0x602088-0x6034a0) and we have our address. The value is pretty simple too. We just need to zero out the \0x90 byte. Be careful here though ascii '0' won't solve the challenge.

I was able to get the flag from the terminal running heapedit locally by sending a EOF but <ins>solve.py</ins> is cleaner and works for the picoctf server.

![flag](img/flag_cache.png)

### Conclusion
This was a fun one. I learned a little about the heap implementation of this malloc and got to practice a heap exploitation technique against it. Also, continued to learn some cool tools like pwninit and gef. Thanks for reading!